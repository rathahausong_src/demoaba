﻿using System.Linq;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;
using Xunit;

namespace XUnitUserIntegration
{
    public class ListUserServiceTest
    {
        private readonly ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> repositoryOperation;

        public ListUserServiceTest()
        {
            this.serviceOperationExecutor = new ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>();
            this.repositoryOperation = new ListUserImp();
        }

        [Fact]
        public void ListAllUserWithPaginationTest() {
            const string actionLog = "test_list_user";
            var request = new RequetListUser()
            {
                Pagination = new RequestPagination()
                {
                    Page = 1,
                    Limt = 6
                }
            };
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            Assert.Equal(errorCode, "200");
            var users = response.Response.Data.Records;
            Assert.NotNull(users);
            var totalRecode = users.Count();
            Assert.Equal(totalRecode, 6);
        }


        [Fact]
        public void ListAllUserNotFoundWithPaginationTest()
        {
            const string actionLog = "test_list_user";
            var request = new RequetListUser()
            {
                Pagination = new RequestPagination()
                {
                    Page = 2,
                    Limt = 6
                }
            };
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            Assert.Equal(errorCode, "404");
            var users = response.Response.Data.Records;
            Assert.NotNull(users);
            var totalRecode = users.Count();
            Assert.Equal(totalRecode, 0);
        }

    }
}
