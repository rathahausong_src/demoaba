﻿
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;
using Xunit;

namespace XUnitUserIntegration
{
    public class UserLoginServiceTest
    {
        private readonly ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>> repositoryOperation;

        public UserLoginServiceTest()
        {
            serviceOperationExecutor = new ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>>();
            repositoryOperation = new UserLoginImp();
        }
        
        [Fact]
        public void UserLoginWithUsernameAndPassword()
        {
            const string actionLog = "test_user_login";
            var response = UserLogin("simratha", "123", actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "200");
            Assert.NotNull(userInfo);
        }

        [Fact]
        public void FailUserLoginWithUsernameAndPassword()
        {
            const string actionLog = "test_user_login";
            var response = UserLogin("simratha", "00993", actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "404");
            Assert.Null(userInfo);
        }


        private RequestResponse<RequestUserLogin, User> UserLogin(string username, string password, string actionLog)
        {
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, new RequestUserLogin() { username = username, password = password }, actionLog);
            return response;
        }
    }
}
