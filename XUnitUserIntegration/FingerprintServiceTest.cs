﻿using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;
using Xunit;

namespace XUnitUserIntegration
{
    public class FingerprintServiceTest
    {
        private readonly ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> repositoryOperation;
        public FingerprintServiceTest()
        {
            this.serviceOperationExecutor = new ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>();
            this.repositoryOperation = new FingerprintLoginImp();
        }

        [Fact]
        public void FingerprintLoginTest()
        {
            const string actionLog = "test_fingerprint_Login";
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, new RequestFingerprintLogin() { fingerprint = "123456789"}, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "200");
            Assert.NotNull(userInfo);
        }

        [Fact]
        public void FailFingerprintLoginTest()
        {
            const string actionLog = "test_fingerprint_Login";
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, new RequestFingerprintLogin() { fingerprint = "32324324" }, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "404");
            Assert.Null(userInfo);
        }
    }
}
