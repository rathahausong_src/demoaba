﻿
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;
using Xunit;

namespace XUnitUserIntegration
{
    public class UserProfileServiceTest
    {
        private readonly ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestUserProfile, RequestResponse<RequestUserProfile, User>> repositoryOperation;

        public UserProfileServiceTest()
        {
            this.serviceOperationExecutor = new ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>>();
            this.repositoryOperation = new GetUserProfileImp();
        }

        [Fact]
        public void loadUserProfile()
        {
            const string actionLog = "test_load_user_profile";
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, new RequestUserProfile() { loginToken = "123e4567-e89b-12d3-a456-426655440004" }, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "200");
            Assert.NotNull(userInfo);
        }

        [Fact]
        public void loadFailUserProfile()
        {
            const string actionLog = "test_load_fail_user_profile";
            var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, new RequestUserProfile() { loginToken = "123e4567-e89b-12d3-a456-426655440014" }, actionLog);
            Assert.NotNull(response);
            var errorCode = response.Response.ErrorCode;
            var userInfo = response.Response.Data;
            Assert.Equal(errorCode, "404");
            Assert.Null(userInfo);
        }
    }
}
