﻿
using System.Collections.Generic;

namespace UserAPI.Prototypes
{
    public class RecordsWithPagination<R>
    {
        public List<R> Records { get; set; }
        public Pagination Pagination { get; set; }
    }
}
