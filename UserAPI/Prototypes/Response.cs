﻿
using System;

namespace UserAPI.Prototypes
{
    public class Response<RS>
    {
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public RS Data { get; set; }

        public static implicit operator Response<RS>(Response<object> v)
        {
            throw new NotImplementedException();
        }
    }
}
