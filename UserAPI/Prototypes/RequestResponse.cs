﻿
namespace UserAPI.Prototypes
{
    public class RequestResponse<R, RS>
    {
        public R Request { set; get; }
        public Response<RS> Response { set; get; }
    }
}
