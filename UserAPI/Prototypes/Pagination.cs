﻿
namespace UserAPI.Prototypes
{
    public class Pagination
    {
        public int Page { get; set; }
        public int Limt { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
    }
}
