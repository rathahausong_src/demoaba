﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using UserAPI.Repositories;

namespace UserAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.ListAllUsers();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
