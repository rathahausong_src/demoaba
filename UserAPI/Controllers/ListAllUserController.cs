﻿
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;

namespace UserAPI.Controllers
{

    [Authorize]
    [Route("api/list/user")]
    [ApiController]
    public class ListAllUserController : BaseController<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>
    {

        private readonly ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> repositoryOperation;
        private readonly IValidator<RequetListUser> listUserValidator;

        public ListAllUserController(ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> serviceOperationExecutor,
            IRepositoryOperation<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>> repositoryOperation,
             IValidator<RequetListUser> listUserValidator)
        {
            this.serviceOperationExecutor = serviceOperationExecutor;
            this.repositoryOperation = repositoryOperation;
            this.listUserValidator = listUserValidator;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        public override ActionResult<RequestResponse<RequetListUser, RecordsWithPagination<User>>> ExecuteRequest([FromQuery] RequetListUser request)
        {
            const string actionLog = "list_all_user";
            var validationResult = listUserValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return ResponseBadRequestAction(request, validationResult);
            }
            dynamic response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            return ResponseAction(response);
        }
    }
}