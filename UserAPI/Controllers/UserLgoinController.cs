﻿using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;

namespace UserAPI.Controllers
{

    [Authorize]
    [Route("api/user/login")]
    [ApiController]
    public class UserLgoinController : BaseController<RequestUserLogin, RequestResponse<RequestUserLogin, User>>
    {

        private readonly ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>> repositoryOperation;
        private readonly IValidator<RequestUserLogin> userLoginValidator;

        public UserLgoinController(ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>> serviceOperationExecutor,
            IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>> repositoryOperation,
            IValidator<RequestUserLogin> userLoginValidator)
        {
            this.serviceOperationExecutor = serviceOperationExecutor;
            this.repositoryOperation = repositoryOperation;
            this.userLoginValidator = userLoginValidator;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        public override ActionResult<RequestResponse<RequestUserLogin, User>> ExecuteRequest([FromBody] RequestUserLogin request)
        {
            const string actionLog = "user_login";
            var validationResult = userLoginValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return ResponseBadRequestAction(request, validationResult);
            }
            dynamic response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            return this.ResponseAction(response);
        }
    }
}