﻿using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;

namespace UserAPI.Controllers
{

    [Authorize]
    [Route("api/fingerprint/login")]
    [ApiController]
    public class FingerprintLoginController : BaseController<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>
    {
        private readonly ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> repositoryOperation;
        private readonly IValidator<RequestFingerprintLogin> fingerprintLoginValidator;

        public FingerprintLoginController(ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> serviceOperationExecutor,
            IRepositoryOperation<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>> repositoryOperation,
            IValidator<RequestFingerprintLogin> fingerprintLoginValidator)
        {
            this.serviceOperationExecutor = serviceOperationExecutor;
            this.repositoryOperation = repositoryOperation; 
            this.fingerprintLoginValidator = fingerprintLoginValidator;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        public override ActionResult<RequestResponse<RequestFingerprintLogin, User>> ExecuteRequest([FromBody] RequestFingerprintLogin request)
        {
            const string actionLog = "fingerprint_Login";
            var validationResult = fingerprintLoginValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return ResponseBadRequestAction(request, validationResult);
            }
            dynamic response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            return this.ResponseAction(response);
        }
    }
}