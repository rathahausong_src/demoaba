﻿
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UserAPI.Prototypes;

namespace UserAPI.Controllers
{
    public abstract class BaseController<R, RS> : ControllerBase
    {
        public abstract ActionResult<RS> ExecuteRequest(R request);
        protected virtual ActionResult ResponseAction(RS response)
        {

            var resultStr = JsonConvert.SerializeObject(response);
            var data = JsonConvert.DeserializeObject<RequestResponse<object, object>>(resultStr);
            var status = data.Response.ErrorCode;
            switch (status)
            {
                case "200": return Ok(response);
                case "201": return Accepted(response);
                case "400": return BadRequest(response);
                case "401": return Unauthorized();
                case "404": return NotFound(response);
                default: return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        protected virtual ActionResult ResponseBadRequestAction(R request, ValidationResult validation)
        {
            RequestResponse<R, ValidationResult> rs = new RequestResponse<R, ValidationResult>();
            rs.Request = request;
            rs.Response = new Response<ValidationResult>() {
                ErrorCode = "400",
                Message = "Bad request",
                Data = validation
            };
            return BadRequest(rs);
        }
    }
}
