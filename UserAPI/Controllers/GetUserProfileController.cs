﻿using System.Net.Mime;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;

namespace UserAPI.Controllers
{

    [Authorize]
    [Route("api/get/user/profile")]
    [ApiController]
    public class GetUserProfileController : BaseController<RequestUserProfile, RequestResponse<RequestUserProfile, User>>
    {
        private readonly ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestUserProfile, RequestResponse<RequestUserProfile, User>> repositoryOperation;
        private readonly IValidator<RequestUserProfile> userProfileValidator;

        public GetUserProfileController(ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>> serviceOperationExecutor,
            IRepositoryOperation<RequestUserProfile, RequestResponse<RequestUserProfile, User>> repositoryOperation,
            IValidator<RequestUserProfile> userProfileValidator)
        {
            this.serviceOperationExecutor = serviceOperationExecutor;
            this.repositoryOperation = repositoryOperation;
            this.userProfileValidator = userProfileValidator;
        }

        
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        public override ActionResult<RequestResponse<RequestUserProfile, User>> ExecuteRequest([FromQuery] RequestUserProfile request)
        {
            const string actionLog = "user_profile";
            var validationResult = userProfileValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return ResponseBadRequestAction(request, validationResult);
            }
            dynamic response = serviceOperationExecutor.ExecuteOperation(repositoryOperation, request, actionLog);
            return this.ResponseAction(response);
        }
    }
}