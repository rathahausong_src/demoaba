﻿namespace UserAPI.Models
{
    public class User
    {
        public string username { set; get; }
        public string password { set; get; }
        public string address { set; get; }
        public string role { set; get; }
        public string avartar { set; get; }
        public string loginToken { set; get; }
        public string fingerprint { set; get; }
    }
}
