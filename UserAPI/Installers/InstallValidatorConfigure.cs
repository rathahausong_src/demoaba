﻿
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using UserAPI.RequestModels;
using UserAPI.Validators;

namespace UserAPI.Installers
{
    public static class InstallValidatorConfigure
    {
        public static void InstallerConfigureServices(IServiceCollection services) {

            services.AddTransient<IValidator<RequestUserLogin>, UserLoginValidator>();
            services.AddTransient<IValidator<RequestFingerprintLogin>, FingerprintLoginValidator>();
            services.AddTransient<IValidator<RequetListUser>, ListUserValidator>();
            services.AddTransient<IValidator<RequestUserProfile>, UserProfileValidator>();
        }
    }
}
