﻿
using Microsoft.Extensions.DependencyInjection;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;

namespace UserAPI.Installers
{
    public static class InstallerSingleTonConfigure
    {
        public static void InstallerConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IRepositoryOperation<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>, ListUserImp>();
            services.AddSingleton<ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>,
                ServiceOperationExecutor<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>>();

            services.AddSingleton<IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>>, UserLoginImp>();
            services.AddSingleton<ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>>,
                ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>>>();

            services.AddSingleton<IRepositoryOperation<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>, FingerprintLoginImp>();
            services.AddSingleton<ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>,
                ServiceOperationExecutor<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>>();

            services.AddSingleton<IRepositoryOperation<RequestUserProfile, RequestResponse<RequestUserProfile, User>>, GetUserProfileImp>();
            services.AddSingleton<ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>>,
                ServiceOperationExecutor<RequestUserProfile, RequestResponse<RequestUserProfile, User>>>();
        }
    }
}
