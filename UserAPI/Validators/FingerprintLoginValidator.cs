﻿
using FluentValidation;
using UserAPI.RequestModels;

namespace UserAPI.Validators
{
    public class FingerprintLoginValidator : AbstractValidator<RequestFingerprintLogin>
    {
        public FingerprintLoginValidator()
        {
            RuleFor(t => t.fingerprint).NotEmpty().MinimumLength(3)
              .WithMessage("fingerprint length can't be more than 3.");
        }
    }
}
