﻿
using FluentValidation;
using System.Text.RegularExpressions;
using UserAPI.RequestModels;

namespace UserAPI.Validators
{
    public class UserProfileValidator : AbstractValidator<RequestUserProfile>
    {
        public UserProfileValidator()
        {
            RuleFor(x => x.loginToken).NotEmpty()
                .Must(guid => GuidValidator.IsGuid(guid.ToString()))
                .WithMessage("loginToken can not be null and format value is UUID");

        }
        private static class GuidValidator
        {
            private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            public static bool IsGuid(string candidate)
            {
                if (candidate != null)
                {
                    if (isGuid.IsMatch(candidate))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
