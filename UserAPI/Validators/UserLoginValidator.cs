﻿
using FluentValidation;
using UserAPI.RequestModels;

namespace UserAPI.Validators
{
    public class UserLoginValidator : AbstractValidator<RequestUserLogin>
    {
        public UserLoginValidator()
        {
            RuleFor(t => t.username).NotEmpty().MinimumLength(3)
               .WithMessage("username length can't be more than 3.");

            RuleFor(t => t.password).NotEmpty().MinimumLength(3)
              .WithMessage("password length can't be more than 3.");
        }
    }
}
