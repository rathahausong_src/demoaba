﻿
using FluentValidation;
using UserAPI.RequestModels;

namespace UserAPI.Validators
{
    public class ListUserValidator : AbstractValidator<RequetListUser>
    {
        public ListUserValidator()
        {
            RuleFor(t => t).NotNull()
                .WithMessage("List user is required")
                .WithMessage("List user can not null");

            RuleFor(t => t.Pagination.Page).NotNull().NotEmpty()
               .WithMessage("Page is required")
               .WithMessage("Page can not be null or zero number");

            RuleFor(t => t.Pagination.Limt).NotNull().NotEmpty()
               .WithMessage("Limt is required")
               .InclusiveBetween(1, 100)
               .WithMessage("Limt can not be null or zero number and can not large then 100");
        }
    }
}
