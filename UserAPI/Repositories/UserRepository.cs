﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UserAPI.Models;

namespace UserAPI.Repositories
{
    public class UserRepository
    {

        private string ReadDb()
        {
            var currentPath = Directory.GetCurrentDirectory();
            var body = File.ReadAllText(currentPath + "\\db\\db.json");
            return body;
        }

        public List<User> ListAllUsers()
        {
            var body = ReadDb();
            List<User> listUser = JsonConvert.DeserializeObject<List<User>>(body);
            return listUser;
        }
    }
}
