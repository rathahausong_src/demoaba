﻿
using System.ComponentModel;

namespace UserAPI.RequestModels
{
    public class RequestPagination
    {
        [DefaultValue("1")]
        public int Page { get; set; }
        [DefaultValue("2")]
        public int Limt { get; set; }
    }
}
