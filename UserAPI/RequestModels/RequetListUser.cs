﻿
using System.ComponentModel.DataAnnotations;

namespace UserAPI.RequestModels
{
    public class RequetListUser
    {
        [Required]
        public RequestPagination Pagination { get; set; }
    }
}
