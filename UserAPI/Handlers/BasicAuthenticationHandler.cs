﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.RequestModels;
using UserAPI.Service;
using UserAPI.Service.Command;
using UserAPI.Service.UserServiceImp;

namespace UserAPI.Handlers
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>> serviceOperationExecutor;
        private readonly IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>> repositoryOperation;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            this.serviceOperationExecutor = new ServiceOperationExecutor<RequestUserLogin, RequestResponse<RequestUserLogin, User>>();
            this.repositoryOperation = new UserLoginImp();
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            //username: ratha
            //password: @WSXAQ!@&#*&^@&!^@*@@&^#*&!&*&&#&*T@*$T@*$
            //key header: cmF0aGE6QFdTWEFRIUAmIyomXkAmIV5AKkBAJl4jKiYhJiomJiMmKlRAKiRUQCok
            const string actionLog = "authentication_handler";

            try
            {
                
                if (!Request.Headers.ContainsKey("Authorization"))
                {
                    return AuthenticateResult.Fail("Authorization header was not found");
                }
                var authenticationHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var bytes = Convert.FromBase64String(authenticationHeaderValue.Parameter);
                string[] credentials = Encoding.UTF8.GetString(bytes).Split(":");
                string username = credentials[0];
                string password = credentials[1];

                var response = serviceOperationExecutor.ExecuteOperation(repositoryOperation,
                    new RequestUserLogin { username = username, password = password },
                    actionLog);

                if (response.Response.ErrorCode == "404")
                {
                    return AuthenticateResult.Fail("Invalid username or password");
                }
                else if (response.Response.ErrorCode == "200")
                {
                    var user = response.Response.Data;
                    if (user.role == "Api")
                    {
                        var claims = new[] { new Claim(ClaimTypes.Name, user.username) };
                        var identity = new ClaimsIdentity(claims, Scheme.Name);
                        var principal = new ClaimsPrincipal(identity);
                        var ticket = new AuthenticationTicket(principal, Scheme.Name);
                        return AuthenticateResult.Success(ticket);
                    }else
                    {
                        return AuthenticateResult.Fail("Invalid role");
                    }
                }
            }
            catch(Exception ex) { 
                
            }
            return AuthenticateResult.Fail("Error has occured");
        }
    }
}
