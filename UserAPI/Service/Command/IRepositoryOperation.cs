﻿
namespace UserAPI.Service.Command
{
    public interface IRepositoryOperation<R, RS> where R : class
                                                 where RS: class
    {
        RS Execute(R request,string actionLog);
    }
}
