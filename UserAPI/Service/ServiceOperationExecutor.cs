﻿using UserAPI.Service.Command;

namespace UserAPI.Service
{
    public class ServiceOperationExecutor<R, RS> where R : class
                                                 where RS: class
    {
        public RS ExecuteOperation(IRepositoryOperation<R, RS> operation, R request, string actionLog)
        {
            return operation.Execute(request, actionLog);
        }
    }
}
