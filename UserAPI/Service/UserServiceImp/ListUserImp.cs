﻿using System;
using System.Linq;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.Repositories;
using UserAPI.RequestModels;
using UserAPI.Service.Command;

namespace UserAPI.Service.UserServiceImp
{
    public class ListUserImp : IRepositoryOperation<RequetListUser, RequestResponse<RequetListUser, RecordsWithPagination<User>>>
    {
        private UserRepository userRepository;
        public ListUserImp()
        {
            this.userRepository = new UserRepository();
        }

        public RequestResponse<RequetListUser, RecordsWithPagination<User>> Execute(RequetListUser request, string actionLog)
        {
            var page = request.Pagination.Page;
            var limit = request.Pagination.Limt;

            var listUser = userRepository.ListAllUsers();
            var totalRecords = listUser.Count();
            var totalPages = (int)Math.Ceiling((double)totalRecords / limit);
            var records = listUser.Skip(limit * (page - 1)).Take(limit).ToList();

            var requestResponse = new RequestResponse<RequetListUser, RecordsWithPagination<User>>();
            var response = new Response<RecordsWithPagination<User>>();
            if (records == null || !records.Any())
            {
                response.ErrorCode = "404";
                response.Message = "User Not Found";
                requestResponse.Request = request;
                requestResponse.Response = response;
                response.Data = new RecordsWithPagination<User>()
                {
                    Records = records,
                    Pagination = new Pagination()
                    {
                        Page = page,
                        Limt = limit,
                        TotalRecords = totalRecords,
                        TotalPages = totalPages
                    }
                };
                return requestResponse;
            }
            response.ErrorCode = "200";
            response.Message = "User Found";
            response.Data = new RecordsWithPagination<User>() 
            {
                Records = records,
                Pagination = new Pagination() {
                    Page = page,
                    Limt = limit,
                    TotalRecords = totalRecords,
                    TotalPages = totalPages
                }
            };
            requestResponse.Request = request;
            requestResponse.Response = response;
            return requestResponse;
        }
    }
}
