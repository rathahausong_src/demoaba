﻿using System.Linq;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.Repositories;
using UserAPI.RequestModels;
using UserAPI.Service.Command;

namespace UserAPI.Service.UserServiceImp
{
    public class UserLoginImp : IRepositoryOperation<RequestUserLogin, RequestResponse<RequestUserLogin, User>>
    {
        private UserRepository userRepository;
        public UserLoginImp()
        {
            this.userRepository = new UserRepository();
        }

        public RequestResponse<RequestUserLogin, User> Execute(RequestUserLogin request, string actionLog)
        {
            var listUser = userRepository.ListAllUsers();
            var userName = request.username;
            var password = request.password;

            var userInfo = listUser.SingleOrDefault(u => u.username == userName && u.password == password);
            var requestResponse = new RequestResponse<RequestUserLogin, User>();
            var response = new Response<User>();
            if (userInfo == null)
            {
                response.ErrorCode = "404";
                response.Message = "User Not Found";
                requestResponse.Request = request;
                requestResponse.Response = response;
                return requestResponse;
            }
            response.ErrorCode = "200";
            response.Message = "User Found";
            response.Data = userInfo;
            requestResponse.Request = request;
            requestResponse.Response = response;
            return requestResponse;
        }
    }
}
