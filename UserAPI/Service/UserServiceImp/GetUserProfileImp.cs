﻿using System.Linq;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.Repositories;
using UserAPI.RequestModels;
using UserAPI.Service.Command;

namespace UserAPI.Service.UserServiceImp
{
    public class GetUserProfileImp : IRepositoryOperation<RequestUserProfile, RequestResponse<RequestUserProfile, User>>
    {
        private UserRepository userRepository;
        public GetUserProfileImp()
        {
            this.userRepository = new UserRepository();
        }

        public RequestResponse<RequestUserProfile, User> Execute(RequestUserProfile request, string actionLog)
        {
            var listUser = userRepository.ListAllUsers();
            var loginToken = request.loginToken;
            var userInfo = listUser.AsParallel()
                .Select(f => new User() {username= f.username, role = f.role, loginToken = f.loginToken, address = f.address})
                .ToList().SingleOrDefault(u => u.loginToken == loginToken);

            var requestResponse = new RequestResponse<RequestUserProfile, User>();
            var response = new Response<User>();
            if (userInfo == null)
            {
                response.ErrorCode = "404";
                response.Message = "User Not Found";
                requestResponse.Request = request;
                requestResponse.Response = response;
                return requestResponse;
            }
            response.ErrorCode = "200";
            response.Message = "User Found";
            response.Data = userInfo;
            requestResponse.Request = request;
            requestResponse.Response = response;
            return requestResponse;
        }
    }
}
