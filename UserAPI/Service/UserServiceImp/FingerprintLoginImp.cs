﻿using System.Linq;
using UserAPI.Models;
using UserAPI.Prototypes;
using UserAPI.Repositories;
using UserAPI.RequestModels;
using UserAPI.Service.Command;

namespace UserAPI.Service.UserServiceImp
{
    public class FingerprintLoginImp : IRepositoryOperation<RequestFingerprintLogin, RequestResponse<RequestFingerprintLogin, User>>
    {
        private UserRepository userRepository;
        public FingerprintLoginImp()
        {
            this.userRepository = new UserRepository();
        }
        public RequestResponse<RequestFingerprintLogin, User> Execute(RequestFingerprintLogin request, string actionLog)
        {
            var listUser = userRepository.ListAllUsers();
            var fingerprint = request.fingerprint;

            var userInfo = listUser.SingleOrDefault(u => u.fingerprint == fingerprint);
            var requestResponse = new RequestResponse<RequestFingerprintLogin, User>();
            var response = new Response<User>();
            if (userInfo == null)
            {
                response.ErrorCode = "404";
                response.Message = "User Not Found";
                requestResponse.Request = request;
                requestResponse.Response = response;
                return requestResponse;
            }
            response.ErrorCode = "200";
            response.Message = "User Found";
            response.Data = userInfo;
            requestResponse.Request = request;
            requestResponse.Response = response;
            return requestResponse;
        }
    }
}
